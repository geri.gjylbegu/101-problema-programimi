#### Kërkesa

Vogëlushja Meli i ka qejf balonat me ngjyra dhe mami i ka blerë për
ditëlindje disa balona me ngjyra të ndryshme. Mirëpo Meli do të
dëshironte ti kishte të gjitha balonat me të njëjtën ngjyrë, kështu që
mori kolorat dhe ju fut punës për ti ngjyrosur.

Bëni një program që gjen sa është numri më i vogël i balonave që duhen
ngjyrosur, për ti bërë të gjitha me të njëjtën ngjyrë.

##### Shembull

```bash
$ cat input.txt
3
abc
bb
baabacd

$ python3 prog.py < input.txt
2
0
4
```

Çdo ngjyrë shënohet me një shkronjë të veçantë. Në rastin e parë
mjafton të ngjyrosim balonat **b** dhe **c** me ngjyrën **a**.  Në
rastin e dytë të gjitha balonat kanë të njëjtën ngjyrë. Kurse në
rastin e tretë balonat me ngjyrat **b**, **c** dhe **d** duhet ti
ngjyrosim me ngjyrën **a**.

#### Zgjidhja 1

```python
for _ in range(int(input())):
    str = input()
    nr = {}    # fjalor per shkronjat dhe numrin e seciles shkronje
    for c in str:
        nr[c] = nr.get(c, 0) + 1
    print(len(str) - max(nr.values()))
```

##### Sqarime

Gjemë numrin e balonave të secilës ngjyrë, dhe pastaj nga numri i
gjithë balonave heqim numrin e balonave që janë më shumë.

#### Zgjidhja 2

```python
for _ in range(int(input())):
    str = input()
    print(len(str) - max([str.count(c) for c in set(str)]))
```
