#### Kërkesa

Një pikë shitje bën zbritje 10% nëse sasia e mallit të blerë është mbi 1000 copë.
Bëni një program që llogarit vlerën e blerjes, duke ditur sasinë dhe çmimin.

Referenca: https://www.codechef.com/problems/FLOW009

##### Shembull

```bash
$ cat input.txt
3
100 120
10 20
1200 20

$ python3 prog.py < input.txt
12000.000000
200.000000
21600.000000
```

#### Zgjidhja

```python
T = int(input())
for t in range(T):
    sasia, cmimi = map(int, input().split())
    vlera = sasia * cmimi
    if sasia > 1000:
        vlera -= vlera / 10
    print(vlera)
```
