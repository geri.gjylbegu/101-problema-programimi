#### Kërkesa

Jepet një numër natyror. Gjeni shumën e shifrës së parë dhe të fundit të tij.

Referenca: https://www.codechef.com/problems/FLOW004

##### Shembull

```bash
$ cat input.txt
3
1234
124894
242323

$ python3 prog.py < input.txt
5
5
5
```

#### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    n = input()
    d1 = int(n[0])
    d2 = int(n) % 10
    print(d1 + d2)
```

#### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    n = input()
    s1 = n[0]
    s2 = n[len(n) - 1]
    print(int(s1) + int(s2))
```

#### Zgjidhja 3

```python
T = int(input())
for t in range(T):
    n = input()
    print(int(n[0]) + int(n[-1]))
```

##### Sqarime

Numrin e lexojmë si string në `n`. Shifra e parë është `n[0]` kurse
shifra e fundit është `n[-1]`. Por këto janë si *varg* (*string*). Që
të mund ti mbledhim duhet ti kthejmë më parë në numra të plotë
(*integer*).
