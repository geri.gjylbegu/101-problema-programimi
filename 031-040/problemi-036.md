#### Kërkesa

Një varg shkronjash quhet palindorm nëse duke u lexuar nga fillimi në
fund dhe nga fundi në fillim është njësoj. Për shembull emri ANA nëse
lexohet mbrapsht do të jetë përsëri e njëjta fjalë. Bëni një program
që kontrollon nëse një varg i dhënë është palindrom.

Referenca: https://www.codechef.com/problems/PALL01

##### Shembull

```bash
$ cat input.txt
5
6666
123321
kapak
ana
palindrom

$ python3 prog.py < input.txt
Yes
Yes
Yes
Yes
No
```

#### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    S = input()
    n = len(S)
    for i in range(n):
        if S[i] != S[n-i-1]:
            print('no')
            break
    else:
        print('yes')
```

##### Sqarime

Për çdo shkronjë kontrollojmë nëse është e njëjtë me shkronjën
korresponduese nga fundi.

#### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    S = input()
    n = len(S)
    i = 0
    while i < n:
        if S[i] != S[n-i-1]:
            print('no')
            break
        i += 1
    else:
        print('yes')
```

##### Sqarime

Zgjidhje e njëjtë me të parën, vetëm se në vend të for-it përdorim
ciklin while.

##### Zgjidhja 3

```python
T = int(input())
for t in range(T):
    S = input()
    n = len(S)
    i = 0
    j = n-1
    while  i < j:
        if S[i] != S[j]:
            print('no')
            break
        i += 1
        j -= 1
    else:
        print('yes')
```

##### Sqarime

Në këtë zgjidhje përdorim 2 indekse, një që niset nga fillimi dhe një
që niset nga fundi. Cikli ndërpritet kur të dy indekset takohen në mes
të vargut.

#### Zgjidhja 4

```python
for _ in range(int(input())):
    S = input()
    l1 = list(S)
    l2 = list(S)
    l2.reverse()
    print('Yes') if l1 == l2 else print('No')
```

##### Sqarime

Në këtë zgjidhje përdorim funksionin `reverse()` të listës që e kthen
një listë në renditjen e kundërt.

#### Detyra

Në kohët e lashta, komandanti i një ushtrie ishte supersticioz. Ai e
quante një ushtar "me fat" nëse numri i armëve që kishte ushtari ishte
çift dhe "pa fat" në të kundërt. Ushtrinë e quante "gati për betejë"
nëse numri i ushtarëve me fat ishte më i madh se ai i ushtarëve pa
fat. Në të kundërt e quante "jo gati".

Nëse ju jepet numri i armëve që ka çdo ushtar, bëni një program që
gjen nëse ushtria është gati për betejë ose jo.

Referenca: https://www.codechef.com/problems/AMR15A

##### Shembull

```bash
$ cat input.txt
5
1
1
1
2
4
11 12 13 14
3
2 3 4
5
1 2 3 4 5

$ python3 prog.py < input.txt
NOT READY
READY FOR BATTLE
NOT READY
READY FOR BATTLE
NOT READY
```
