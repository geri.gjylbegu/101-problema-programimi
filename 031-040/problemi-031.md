#### Kërkesa

Në një varg shkronjash, shënja `?` mund të zëvendësohet me çdo lloj
shkronje tjetër. Gjeni nëse dy vargje të dhëna, me gjatësi të njëjtë,
janë të përputhshëm me njëri-tjetrin (dmth nëse mund të bëhen të
barabartë duke zëvendësuar shenjat `?` me shkronja).

Referenca: https://www.codechef.com/problems/TWOSTR

##### Shembull

```bash
$ cat input.txt
2
s?or?
sco??
stor?
sco??

$ python3 prog.py < input.txt
Yes
No
```

Në rastin e parë, fjala `score` përputhet me të dyja vargjet e dhëna.
Kurse në rastin e dytë nuk është e mundur të përputhen.

#### Zgjidhja 1

```python
for _ in range(int(input())):
    s1 = input()
    s2 = input()
    for i in range(len(s1)):
        if s1[i] != '?' and s2[i] != '?' and s1[i] != s2[i]:
            print('No')
            break
    else:
        print('Yes')
```

#### Zgjidhja 2

```python
def perputhet(s1, s2):
    for i in range(len(s1)):
        if s1[i] != '?' and s2[i] != '?' and s1[i] != s2[i]:
            return 'No'
    return 'Yes'

for _ in range(int(input())):
    s1 = input()
    s2 = input()
    print(perputhet(s1, s2))
```

#### Detyra

Vogëlushja Meli i ka qejf balonat me ngjyra dhe mami i ka blerë për
ditëlindje disa balona me ngjyra të kuqe dhe blu. Mirëpo Meli do të
dëshironte ti kshte të gjitha balonat me të njëjtën ngjyrë, kështu që
mori kolorat dhe ju fut punës për ti ngjyrosur.

Bëni një program që gjen sa është numri më i vogël i balonave që duhen
ngjyrosur, për ti bërë të gjitha me të njëjtën ngjyrë.

Referenca: https://www.codechef.com/problems/CHN09

##### Shembull

```bash
$ cat input.txt
3
ab
bb
baaba

$ python3 prog.py < input.txt
1
0
2
```

Balonat e kuqe i kemi shënuar me **a**, blutë me **b**. Në rastin e
parë mjafton të ngjyrosim një balonë të kuqe me blu, ose një blu me të
kuqe. Në rastin e dytë të gjitha balonat kanë të njëjtën ngjyrë. Kurse
në rastin e tretë mjafton të ngjyrosim dy balonat blu me të kuqe.
