#### Kërkesa

Tre shokë, le ti quajmë A, B dhe C, bëjnë pohimet e mëposhtme:
- A: Unë kam $`x`$ lekë më shumë ose më pak se B
- B: Unë kam $`y`$ lekë më shumë ose më pak se C
- C: Unë kam $`z`$ lekë më shumë ose më pak se A

Bëni një program që merr numrat $`x, y, z`$ dhe gjen nëse A, B dhe C
mund të kenë sasi lekësh të tilla që të gjitha pohimet të jenë të
vërteta.

Referenca: https://www.codechef.com/problems/THREEFR

##### Shembull

```bash
$ cat input.txt
2
1 2 1
1 1 1

$ python3 prog.py < input.txt
yes
no
```

#### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    x, y, z = map(int, input().split())
    if x >= y and x >= z:
        print('YES' if x == y + z else 'NO')
    elif y >= x and y >= z:
        print('YES' if y == x + z else 'NO')
    else:
        print('YES' if z == x + y else 'NO')
```

##### Sqarime

Që pohimet të jenë të vërteta, duhet që më i madhi prej numrave të
jetë i barabartë me shumën e dy të tjerëve.

#### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    x, y, z = map(int, input().split())
    l = [x, y, z]
    l.sort(reverse=True)
    print('YES' if l[0] == l[1] + l[2] else 'NO')
```

##### Sqarime

Për të gjetur më të madhin, ne mund të krijojmë një listë me ta dhe ti
rendisim në rendin zbritës.

#### Zgjidhja 3

```python
T = int(input())
for t in range(T):
    x, y, z = map(int, input().split())
    if y > x: x,y=y,x
    if z > x: x,z=z,x
    print('YES' if x==y+z else 'NO')
```

##### Sqarime

Mund tu ndërrojmë vendet vlerave, në mënyrë të tillë që te `x`-i të kemi gjithmonë vlerën më të madhe.

#### Zgjidhja 4

```python
for _ in range(int(input())):
    x, y, z = map(int, input().split())
    print('YES' if 2*max(x,y,z)==x+y+z else 'NO')
```

##### Sqarime

Disa zgjidhje janë më të thjeshta dhe më të shkurtra se të tjerat.

### Detyra

Në një restorant ka filluar punë një kamarier i ri. Meqënëse nuk është
dhe aq i mirë në matematikë, ndonjëherë e kthen reston gabim. Shefi i
jep atij një problem të thjeshtë: Sa bëjnë $`a - b`$?  Përgjigja e tij
është gabim. Dhe për çudi ka vetëm një shifër gabim. E merrni dot me
mënd?

A mund të shkruani një program që bën të njëjtin gabim? Programi merr
dy numra `a` dhe `b` dhe duhet të japë një diference të gabuar `a-b`.
Përgjigja duhet të jetë një numër pozitiv që ka të njëjtin numër
shifrash me diferencën e saktë, ku të gjitha shifrat janë njësoj me
diferencën e saktë, me përjashtim të njërës që duhet të jetë e
ndryshme. Zeroja nuk duhet të jetë si shifër e parë (nga e majta).
Nëse ka përgjigje të ndryshme që i plotësojnë këto kushte, mjafton të
jepni vetëm njërën.

Referenca: https://www.codechef.com/problems/CIELAB

##### Shembull

```bash
$ cat input.txt
3
5858 1234
239 230
4375 4375

$ python3 prog.py < input.txt
1624
8
1
```

Në rastin e parë, diferenca e saktë është `5858 - 1234 = 4624`, kështu
që këto përgjigje do ishin të sakta: `2624`, `4324`, `4623`, `4604`,
`4629`, kurse këto do ishin të gabuara: `0624`, `624`, `5858`, `4624`,
`04624`.
