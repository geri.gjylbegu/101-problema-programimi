
### Problemi 003

#### Kërkesa

Bëni një program që merr vlerat e tre këndeve (në gradë) dhe nxjerr
"YES" nëse këto mund të jenë kënde të një trekëndëshi, ose "NO" nëse
nuk mund të jenë.

Referenca: https://www.codechef.com/problems/FLOW013

##### Shembull

```bash
$ cat input.txt
3
40 40 100
45 45 90
180 1 1

$ python3 p003.py < input.txt
YES
YES
NO
```

#### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    a, b, c = input().split()
    if int(a) + int(b) + int(c) == 180:
        print("YES")
    else:
        print("NO")
```

##### Sqarime

Këndet e një trekëndëshi e kanë shumën 180 gradë.

Meqënëse numrat i lexojmë si *varg* (*string*), kemi përdorur
funksionin `int()` për ti kthyer në numra (përndryshe nuk mund ti
mbledhim).

#### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    a, b, c = map(int, input().split())
    if a + b + c == 180:
        print("YES")
    else:
        print("NO")
```

##### Sqarime

Funksioni `map()` që është përdorur këtu, zbaton funksionin `int()` te
të gjithë elementet e objektit të listueshëm. Si përfundim, në
ndryshoret `a`, `b` dhe `c` ruhen numra, dhe jo vargje.

### Detyra

Bëni një program që merr 2 numra `a` dhe `b` dhe nxjerr:
- shenjën `<` nëse `a` është më i vogël se `b`
- shenjën `>` nëse `a` është më i madh se `b`
- shenjën `=` nëse `a` është i barabartë me `b`

Referenca: https://www.codechef.com/problems/CHOPRT

##### Shembull

```bash
$ cat input.txt
3
10 20
20 10
10 10

$ python3 p003.3.py < input.txt
≺
≻
=
```
