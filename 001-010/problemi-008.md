#### Kërkesa

Një olimpiadë programimi zhvillohet në disa faza. Nga faza e parë në
të dytën kualifikohen `K` pjesëmarrësit që kanë marrë më shumë
pikë. Por nqs janë disa pjesëmarrës që kanë numër të njëjtë pikësh me
atë që është në vendin e K-të, edhe ata kualifikohen.

Bëni një program që merr numrin K dhe pikët e pjesëmarrësve, dhe
nxjerr numrin e pjesëmarrësve që kualifikohen për në fazën tjetër.

Referenca: https://www.codechef.com/problems/QUALPREL

##### Shembull

```bash
$ cat input.txt
2
5 1
3 5 2 4 5
6 4
6 5 4 3 2 1

$ python3 prog.py < input.txt
2
4
```

#### Zgjidhja

```python
T = int(input())
for t in range(T):
    n, k = map(int, input().split())
    piket = list(map(int, input().split()))
    piket.sort(reverse=True)
    while k < n and piket[k] == piket[k-1]:
        k += 1
    print(k)
```

##### Sqarime

Së pari, pikët e konkurentëve duhen renditur në rendin zbritës
(`piket.sort(reverse=True)`). Pastaj, `k` studentët e parë do
kualifikohen pa diskutim, por gjithashtu edhe studentët pasardhës,
nëse kanë të njëjtin numër pikësh me atë që është në vendin `k`.

Kushti `k < n` siguron që `k`-ja nuk do ta kapërcejë fundin e listës
`piket`. Gjithashtu, gjuha Python (si shumë gjuhë të tjera) bën një
vlerësim të shkurtuar të shprehjeve llogjike, që do të thotë se nëse
kushti `A` i shprehjes `A and B` del `False`, atere nuk kalohet fare
te vlerësimi i shprehjes `B`.

Në rastin tonë, nëse shpreja `piket[k] == piket[k-1]` do të vlerësohej
kur `k==n`, do të nxirrte një gabim (sepse `piket[n]` ndodhet jashtë
kufirit të listës). Por falë vlerësimit të shkurtuar kjo nuk ndodh.

### Detyra

Një olimpiadë programimi është zhvilluar në një qytet në vitet `2010`,
`2015`, `2016`, `2017`, `2019`. Bëni një program që merr disa vite dhe
për secilin prej tyre nxjerr `HOSTED` nëse olimpiada është zhvilluar
atë vit në qytet, ose `NOT HOSTED` nëse nuk është zhvilluar.

Referenca: https://www.codechef.com/problems/SNCKYEAR

##### Shembull

```bash
$ cat input.txt
2
2019
2018

$ python3 prog.py < input.txt
HOSTED
NOT HOSTED
```
