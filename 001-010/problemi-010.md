#### Kërkesa

Kosta e ka qejf numrin **4** sepse ka ca veti shumë
interesante. P.sh.:
- Katra është numri më i vogël i përbërë.
- Grupi më i vogël jo-ciklik ka 4 elementë.
- Katra është numri maksimal i shkallës së ekuacioneve që mund të
  zgjidhen në rrënjë.
- Është një teoremë që thotë se çdo hartë mund të ngjyroset vetëm me 4
  ngjyra të ndryshme, në mënyrë që çdo dy vënde kufitare të mos kenë
  të njëjtën ngjyrë.
- Një teoremë e Lagrange-it thotë se çdo numër natyror mund të
  shprehet si shumë e të shumtën 4 katrorë numrash (numra në fuqi të
  dytë).
- E plot të tjera.

I mahnitur nga fuqitë e këtij numri, Kosta e ka zakon që numëron sa
katra janë në çdo numër. Për ta ndihmuar Kostën, bëni një program që
numëron sa shifra **4** ka në një numër të dhënë.

Referenca: https://www.codechef.com/problems/LUCKFOUR

##### Shembull

```bash
$ cat input.txt
5
447474
228
6664
40
81

$ python3 prog.py < input.txt
4
0
1
1
0
```

#### Zgjidhja 1

```python
T = int(input())
for t in range(T):
    number = list(input())
    k = 0
    for s in number:
        if s == '4': k += 1
    print(k)
```

##### Sqarime

Nqs funksionit `list()` i japim si parametër një varg shkronjash, na
kthen një listë me të gjitha shkronjat përbërëse të këtij vargu.

#### Zgjidhja 2

```python
T = int(input())
for t in range(T):
    number = list(input())
    print(number.count('4'))
```

#### Zgjidhja 3

```python
T = int(input())
for t in range(T):
    number = input()
    k = 0
    for i in range(len(number)):
        if number[i] == '4': k += 1
    print(k)
```

#### Zgjidhja 4

```python
for _ in range(int(input())):
    print(input().count('4'))
```

#### Detyra

Jepet një numër natyror. Gjeni shumën e shifrës së parë dhe të fundit të tij.

Referenca: https://www.codechef.com/problems/FLOW004

##### Shembull

```bash
$ cat input.txt
3
1234
124894
242323

$ python3 prog.py < input.txt
5
5
5
```
