
### Problemi 002

#### Kërkesa

Bëni një program që merr një listë me emra dhe mbiemra dhe i nxjerr në
formatin "Mbiemer, Emer".

##### Shembull

```bash
$ cat input.txt
3
Ada Lovelace
Charles Babbage
Alan Turing

$ python3 p002.py < input.txt
Lovelace, Ada
Babbage, Charles
Turing, Alan
```

Numri që ndodhet në rreshtin e parë të `input.txt` tregon se sa
rreshta me emra vijnë më poshtë.


#### Zgjidhja

```python
T = int(input())
for t in range(T):
    emri, mbiemri = input().split()
    print("{}, {}".format(mbiemri, emri))
```

##### Sqarime

Funksioni `split()`, i cili thirret te një *varg shkronjash*
(*string*), e ndan atë varg në nënvargje sipas vendeve bosh, dhe
krijon me to një objekt të listueshëm. P.sh.:
```bash
$ python3
>>> list('ab cd ef'.split())
['ab', 'cd', 'ef']
>>> quit()
```

Ndryshoret `emri` dhe `mbiemri` marrin vlerë njëkohësisht, me një
vlerëdhënie të shumëfishtë (dyfishtë), ku `emri` merr vlerën e parë të
një objekti të listueshëm, kurse `mbiemri` merr vlerën e dytë. P.sh.:
```bash
$ python3
>>> x, y, z = [10, 20, 30]
>>> x
10
>>> y
20
>>> z
30
>>> quit()
```

Funksioni `format()`, i cili thirret te një *varg*, zëvendëson
vendmbajtësit `{}` brenda vargut me vlerat përkatëse që i jepen
funksionit si parametra.

### Detyra

Bëni një program që merr një listë me çifte numrash dhe nxjerr shumën e tyre.

##### Shembull

```bash
$ cat input.txt
3
5 7
20 12
136 2

$ python3 p002.2.py < input.txt
12
32
138
```
