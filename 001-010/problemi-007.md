#### Kërkesa

Grada e çelikut përcaktohet në bazë to kushteve të mëposhtme:
-   **i**. Fortësia është më e madhe se 50
-  **ii**. Përmbajtja e karbonit është më pak se 0.7
- **iii**. Forca elastike është më e madhe se 5600

Grada është si më poshtë:
- **10** nëse plotësohen të treja kushtet
- **9** nëse plotësohen kushtet (i) dhe (ii)
- **8** nëse plotësohen kushtet (ii) dhe (iii)
- **7** nëse plotësohen kushtet (i) dhe (iii)
- **6** nëse plotësohet vetëm njëri nga kushtet
- **5** nëse asnjë nga kushtet nuk plotësohet

Bëni një program që merr vlerat e fortësisë, përmbajtjes së karbonit
dhe forcës elastike, dhe nxjerr gradën e çelikut.

Referenca: https://www.codechef.com/problems/FLOW014

##### Shembull

```bash
$ cat input.txt
3
53 0.6 5602
45 0 4500
0 0 0

$ python3 prog.py < input.txt
10
6
6
```

#### Zgjidhja

```python
T = int(input())
for t in range(T):
    hardness, carbon, tensile = map(float, input().split())
    k1 = hardness > 50
    k2 = carbon < 0.7
    k3 = tensile > 5600
    if k1 and k2 and k3:
        print(10)
    elif k1 and k2:
        print(9)
    elif k2 and k3:
        print(8)
    elif k1 and k3:
        print(7)
    elif k1 or k2 or k3:
        print(6)
    else:
        print(5)
```

### Detyra

Bëni një program që merr kodin e një anije (si një shkronjë) dhe shfaq
llojin e saj (si emër të plotë). Kodet dhe llojet e anijeve janë si më
poshtë:

| **Kodi** | **Lloji**  |
| :------: | :--------- |
| B ose b  | BattleShip |
| C ose c  | Cruiser    |
| D ose d  | Destroyer  |
| F ose f  | Frigate    |

Referenca: https://www.codechef.com/problems/FLOW010

##### Shembull

```bash
$ cat input.txt
3
B
c
D

$ python3 prog.py < input.txt
BattleShip
Cruiser
Destroyer
```
